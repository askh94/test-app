var express = require('express'); 
var app = express();
var fs = require('fs'); 
var cache = require('memory-cache')


app.get('/getItems', function(req, res){
    fs.readFile(__dirname + "/" + "items.json", 'utf8', function(err, data){
        cache.put('items',data)
        res.send(data); 
    });
})

var server = app.listen(3000, function(){
    var host = server.address().address
    var port = server.address().port
    console.log("REST API demo app listening at http://%s:%s", host, port)})